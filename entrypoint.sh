#!/bin/bash
set -e
nginx
php-fpm7.4 -c /etc/php/7.4/fpm/php-fpm.conf
tail -f /var/log/php7.4-fpm.log
