FROM debian:bullseye
RUN apt update && \
    apt install -y \
        nginx \
        php \
        php-cli \
        php-fpm \
        php-curl \
        php-tidy

WORKDIR /var/www/html
COPY src .
COPY default /etc/nginx/sites-available/default
COPY entrypoint.sh /
RUN mkdir /run/php
RUN echo "clear_env = no" >> /etc/php/7.4/fpm/pool.d/www.conf
RUN sed 's/GP/EGP/' -i /etc/php/7.4/fpm/php.ini
RUN nginx -t

ENV TOKEN=test

ENTRYPOINT ["/entrypoint.sh"]
