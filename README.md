# Calculo de Boleta
Muestra el cálculo de Boletas ya sea de IVA u Honorarios, con ingreso del tipo y monto, mostrando los cálculos ya sea bruto o liquido, retención o impuesto, de la boleta en cuestión.

## Preliminares
- Para obtener el código puede hacerlo clonando el repositorio con ```git clone git@gitlab.com:ra_cl/front-calculo-boleta.git```.
- Se ha dejado un Dockerfile para el código tal cual está en este momento, puede ejecutar en su máquina para generar una imagen con:

  ```bash
  docker build -t front .
  docker run -d --name front -e TOKEN_CALCULO_IMPUESTO=secret -e URL_CALCULO_IMPUESTO=http://url_api_twk --rm front
  ```

- Existe una imagen docker basada en la última version estable del código original que puede obtener haciendo:
  ```bash
  docker pull twkralvear/front_calculo_boleta:latest
  ```

## Requerimientos
- PHP version estable
- Nginx o Apache
- Conexión a la API de cálculo

## Configuraciones
- Acceso a las variables de entorno
- Definir las variables de entorno

## Variables de Entorno
|Variable|Descripción|Valor de ejemplo|
|---|---|---|
|URL_CALCULO_IMPUESTO|URL a la API de cálculo|http://url_api.twk|
|TOKEN_CALCULO_IMPUESTO|token que permite la autorización a la URL anterior|secret-token|
