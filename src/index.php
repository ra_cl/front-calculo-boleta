<?php
include_once 'Page.class.php';
include_once 'Template.class.php';
/*
putenv('URL_CALCULO_IMPUESTO=172.17.0.5');
putenv('TOKEN_CALCULO_IMPUESTO=test');
$_POST=array('valor'=>'50000','impuesto'=>'iva');
//*/

function error($msg)
{
    $tpl = new Core\Template('html/error.html');
    $tpl->llena(array('###error###' => $msg));
    return $tpl->getCode();
}

if (isset($_POST['valor'])&&isset($_POST['impuesto'])){
    $url = getenv('URL_CALCULO_IMPUESTO').
        '?valor='.$_POST['valor'].
        '&impuesto='.$_POST['impuesto'].
        '&token='.getenv('TOKEN_CALCULO_IMPUESTO');
    $page = Core\Page::get($url);
    if (empty($page['error'])) {
        $resp = json_decode($page['body'],true);
        //print_r($resp);
        if ($resp['exito']) {
            $msg = array(
                '###concepto###' => $_POST['impuesto'],
                '###liquido2###' => $_POST['valor'],
                '###ret2###' => $resp['bruto']['retencion'],
                '###bruto2###' => $resp['bruto']['valor'],
                '###liquido1###' => $resp['liquido']['valor'],
                '###ret1###' => $resp['liquido']['retencion'],
                '###bruto1###' => $_POST['valor'],
            );
            $tpl = new Core\Template('html/calculo_impuesto.html');
            $tpl->llena($msg);
            echo $tpl->getCode();
        } else {
            echo error($resp['estado']);
        }
    } else {
        echo error($page['error']);
    }
} else {
    $tpl = new Core\Template('html/form_impuesto.html');
    echo $tpl->getCode();
}
