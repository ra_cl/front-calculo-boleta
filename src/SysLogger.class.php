<?php
/**
 * Anotaciones de Log en el log del sistema
 *
 * <code>
 *   \Core\SysLogger::log('mi-sistema','algo fallo inesperadamente',\Core\SysLogger::ERROR);
 * </code>
 *
 * @package Core
 * @filesource SysLogger.class.php
 */

namespace Core;

/**
 * Anotaciones de Log en el log del sistema
 *
 * Esta clase hace uso de openlog y syslog para escribir en el log del sistema,
 * con severidades segun estandard.
 *
 * @author      Roy Alvear <racl@gulix.cl>
 * @version     0.0.1
 * @license     GPL
 * @package     Core
 */
class SysLogger
{

  // Referencia https://httpd.apache.org/docs/2.0/es/mod/core.html#loglevel
  const EMERGENCIA = 1; //600
  const ALERTA = 2; //550
  const CRITICO = 4; //500
  const ERROR = 8; //400
  const ADVERTENCIA = 16; //300
  const NOTICIA = 32; //250
  const INFORMACION = 64; //200
  const DEBUG = 128; //100

  /**
   * Escribir en el log
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   * @param  integer $severidad  Severidad del log
   */
  public static function log($sistema,$mensaje,$severidad)
  {
    $severity=array('1'=>LOG_EMERG,'2'=>LOG_ALERT,'4'=>LOG_CRIT,'8'=>LOG_ERR,'16'=>LOG_WARNING,'32'=>LOG_NOTICE,'64'=>LOG_INFO,'128'=>LOG_DEBUG);
    $mensaje=preg_replace('/\s+/', ' ', print_r($mensaje,true));
    $referencia=pathinfo($_SERVER['SCRIPT_FILENAME'])['basename'].(empty($_SERVER['QUERY_STRING'])?'':'?'.$_SERVER['QUERY_STRING']);
    date_default_timezone_set('America/Santiago');
    $partetiempo = explode(".",microtime(true));
    $ts = $partetiempo[0];
    $ms = isset($partetiempo[1])?$partetiempo[1]:0;
    $fecha='['.date('D M d H:i:s.',$ts).$ms.' '.date('Y',$ts).']';
    $uid='[UID:'.(isset($_SESSION['usuario']['id'])?$_SESSION['usuario']['id']:0).']';
    $client='[client:'.self::obtenerIP().']';
    $pid='[pid '.getmypid().']';
    $tx_severidad=self::severidad($severidad);
    $_SERVER['SERVER_NAME']=isset($_SERVER['SERVER_NAME'])?$_SERVER['SERVER_NAME']:'localhost';
    $serverName = preg_replace('/.*\/\//','',isset($_SESSION['config']['base']['server'])?$_SESSION['config']['base']['server']:$_SERVER['SERVER_NAME']);
    $sessid='[PHPSESSID:'.(isset($_COOKIE['PHPSESSID'])?$_COOKIE['PHPSESSID']:'').']';
    openlog($fecha.'['.$sistema.':'.$tx_severidad.']'.$pid.$client.'['.$serverName.']['.$referencia.']'.$uid.$sessid.' IndapSysLogger', LOG_PERROR, LOG_LOCAL2);
    syslog($severity[$severidad],$mensaje);
    closelog();
  }

  /**
   * Obtiene IP del usuario
   * @return string IP del usuario
   */
   private static function obtenerIP()
   {
      $ip=null;
      if (!empty($_SERVER ['HTTP_X_FORWARDED_FOR'] )) {
        $ip=$_SERVER ['HTTP_X_FORWARDED_FOR'];
      } elseif (!empty($_SERVER ['HTTP_CLIENT_IP'] )) {
          $ip=$_SERVER ['HTTP_CLIENT_IP'];
      } elseif (isset($_SERVER ['REMOTE_ADDR'])) {
          $ip=$_SERVER ['REMOTE_ADDR'];
      }
      return $ip;
   }

  /**
   * Retorna el texto de la severidad indicada
   * @param  integer $severidad  Severidad del log
   * @return string              texto de la severidad
   */
  private static function severidad($severidad)
  {
    $severity=array('1'=>'emerg','2'=>'alert','4'=>'crit','8'=>'error','16'=>'warn','32'=>'notice','64'=>'info','128'=>'debug');
    if (isset($severity[$severidad])) {
      return $severity[$severidad];
    }
    return '';
  }

  /**
   * Escribir en el log y detener el sistema por sveridad emergencia
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function emergencia($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::EMERGENCIA);
    @session_start();
    @session_destroy();
    die($mensaje);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad alerta
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function alerta($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::ALERTA);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad critico
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function critico($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::CRITICO);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad error
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function error($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::ERROR);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad advertencia
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function advetencia($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::ADVERTENCIA);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad noticia
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function noticia($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::NOTICIA);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad informacion
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function info($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::INFORMACION);
  }

  /**
   * Escribir en el log mensaje con nivel de severidad debug
   * @param  string  $sistema    Nombre Identificatorio del Sisitema
   * @param  string  $mensaje    Mensaje de error entregado
   */
  public static function debug($sistema,$mensaje)
  {
    self::log($sistema,$mensaje,self::DEBUG);
  }

}
