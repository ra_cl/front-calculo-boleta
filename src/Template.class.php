<?php
/**
 * Manejo de template por marca
 *
 * Ejemplo
 * <code>
 * $data = array('###title###' => 'mi website', '###content###' => 'Under Construction');
 * $obtpl = new template();
 * $obtpl->setTemplate('/miproyecto/template/index.tpl');
 * $obtpl->llena($data);
 * $obtpl->limpia();
 * echo $obtpl->getCode();
 * </code>
 *
* @package Core
 * @filesource Template.class.php
 */

namespace Core;

/**
 * Manejo de template por marca
 * @package Core
 * @author Roy Alvear <racl@gulix.cl>
 * @license GPL 2
 * @version 1.2.0
 */
class Template
{
    /**
     * @var string  template
     */
    private $template;

    /**
     * constructor de la clase
     *
     * @param string $archi Archivo con su path
     */
    public function __construct( $archi=null )
    {
        $this->template = "";
        if($archi)
            $this->setTemplate($archi);
    }

    /**
     * llenar un template con el arreglo
     *
     * @param array $arreglo
     * @return void
     */
    public function llena($arreglo)
    {
        if(count($arreglo)) {
            foreach($arreglo as $var => $val) {
                if (is_string($val)||is_numeric($val)) {
                    $this->template=str_replace($var,$val,$this->template);
                } else {
                    \Core\SysLogger::error(__CLASS__,$this->lineaResumenInvoca().' El valor "'.$var.'" es un "'.gettype($val).'" y se esperaba un string o numero.');
                }
            }
        }
    }

    /**
     * quienInvoca retorna el nombre de la funcion, archivo y linea de quien realizo la llamada
     * return array
     */
    private function quienInvoca()
    {
      $back=debug_backtrace();
      $total = count($back);
      $quien=$back[0];
      $i=0;
      for($i=0;$i<$total&&(
                    basename($back[$i]['file'])=='MSsqlDB.class.php'||
                    basename($back[$i]['file'])=='DataBase.class.php'||
                    basename($back[$i]['file'])=='MiTabla.class.php'||
                    basename($back[$i]['file'])=='Formulario.class.php'||
                    basename($back[$i]['file'])=='Template.class.php');$i++){
        $quien=$back[$i];
      }
      $quien=$back[$i];
      $quien['file']=basename($quien['file']);
      return $quien;
    }

    /**
     * lineaResumenInvoca, lleva a una linea datos de quien invoca
     * @return string
     */
    private function lineaResumenInvoca()
    {
      $invoca = $this->quienInvoca();
      return $invoca['file'].'->'.$invoca['function'].'('.$invoca['line'].')';
    }

    /**
     * elimina etiquetas no llenadas
     *
     * @param void
     * @return void
     */
    public function limpia()
    {
        $this->template = preg_replace("/###(\w+)(\d*)###/i","",$this->template);
    }

    /**
     * lee un archivo de template y lo carga
     *
     * @param string $archi Archivo con su path
     * @return void
     */
    public function setTemplate($archi)
    {
        if(file_exists($archi)){
            $fil = fopen($archi,"r");
            $cont="";
            while(!feof($fil))
                $cont .= fgets($fil);
            fclose($fil);
            $this->template=$cont;
        } else
            die("No existe ".$archi." inconsistencia!!! ...por favor, avise de este error al webmaster");
    }

    /**
     * Setea un codigo de template
     * @param string $code codigo para el template
     */
    public function setCode($code)
    {
        $this->template = $code;
    }

    /**
     * regresa el codigo
     *
     * @param void
     * @return string
     */
    public function getCode()
    {
        return $this->template;
    }

}
