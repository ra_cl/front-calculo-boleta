<?php
/**
 * comentario
 *
 * @package Core
 * @filesource Page.class.php
 */

namespace Core;

/**
 * comentario
 *
 * @author      Roy Alvear <racl@gulix.cl>
 * @version     0.0.1
 * @license     GPL
 * @package     Core
 */
class Page
{
    /**
     * get, obtiene pagina web por metodo get
     * @param  string   $url    URL a la que se hace referencia
     * @param  array    $header Arreglo de cabeceras
     * @param  integer  $timeout Segundos de espera a la pagina
     * @return array    ('body', 'head' 'cookie')
     */
    static public function get($url, $header=array(), $timeout=10)
    {
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLINFO_HEADER_OUT, true);
        @curl_setopt($curl_handle, CURLOPT_COOKIELIST, true);
        if (count($header)>0) {
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);
        }
        $body=curl_exec($curl_handle);
        $error=curl_error($curl_handle);
        $head=curl_getinfo($curl_handle, CURLINFO_HEADER_OUT);
        $cookie=@curl_getinfo($curl_handle, CURLINFO_COOKIELIST);
        $str_cookie=self::cookieToString($cookie);
        curl_close($curl_handle);
        return array('body'=>$body, 'head'=>$head, 'cookie'=>$str_cookie, 'error'=>$error);
    }

    /**
     * post, obtiene pagina web por metodo post
     * @param  string   $url    URL a la que se hace referencia
     * @param  string   $param  string con la lista de parametros
     * @param  string   $cookie cookie
     * @param  array    $header Arreglo de cabeceras
     * @param  integer  $timeout Segundos de espera a la pagina
     * @return array    ('body', 'head' 'cookie')
     */
    static public function post($url, $param, $cookie='', $header=array(), $timeout=10)
    {
        $curl_handle=curl_init();
        curl_setopt($curl_handle, CURLOPT_URL, $url);
        curl_setopt($curl_handle, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl_handle, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl_handle, CURLINFO_HEADER_OUT, true);
        @curl_setopt($curl_handle, CURLOPT_COOKIELIST, true);
        curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $param);
        if ($cookie!='') {
            curl_setopt ($curl_handle, CURLOPT_COOKIE, $cookie);
        }
        if (count($header)>0) {
            curl_setopt($curl_handle, CURLOPT_HTTPHEADER, $header);
        }
        $body=curl_exec($curl_handle);
        $error=curl_error($curl_handle);
        $head=curl_getinfo($curl_handle, CURLINFO_HEADER_OUT);
        $cookie=@curl_getinfo($curl_handle, CURLINFO_COOKIELIST);
        $str_cookie=self::cookieToString($cookie);
        curl_close($curl_handle);
        return array('body'=>$body, 'head'=>$head, 'cookie'=>$str_cookie, 'error'=>$error);
    }

    /**
     * cookieToString, convierte de un array a un string la cookie
     * @param   array   $cookie     cookie como arreglo
     * @return  string
     */
    static private function cookieToString($cookie)
    {
        $code='';
        if (!empty($cookie))
        foreach ($cookie as $galleta) {
            list($a, $b, $c, $d, $e, $f, $g)=preg_split("/\t/", $galleta);
            $code .= ($code==''?'':'; ')."$f=$g";
        }
        return $code;
    }
}
